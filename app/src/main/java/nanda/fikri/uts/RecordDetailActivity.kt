package nanda.fikri.uts

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import kotlinx.android.synthetic.main.activity_record_detail.*
import java.util.*

class RecordDetailActivity : AppCompatActivity() {

    private var actionBar: ActionBar?=null

    private var dbHelper:MyDbHelper?=null

    private var recordId:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_detail)

        actionBar = supportActionBar
        actionBar!!.title = "Record Details"
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar!!.setDisplayHomeAsUpEnabled(true)

        dbHelper = MyDbHelper(this)

        val intent = intent
        recordId = intent.getStringExtra("RECORD_ID")

        showRecordDetails()
    }

    private fun showRecordDetails(){
        val selectQuery = "SELECT * FROM ${Constants.TABLE_NAME} WHERE ${Constants.C_ID} = \"$recordId\""

        val db = dbHelper!!.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        if(cursor.moveToFirst()){
            do{
                val id = ""+cursor.getInt(cursor.getColumnIndex(Constants.C_ID))
                val judul = ""+cursor.getString(cursor.getColumnIndex(Constants.C_JUDUL))
                val image = ""+cursor.getString(cursor.getColumnIndex(Constants.C_IMAGE))
                val sinopsis = ""+cursor.getString(cursor.getColumnIndex(Constants.C_SINOPSIS))
                val durasi = ""+cursor.getString(cursor.getColumnIndex(Constants.C_DURASI))
                val genre = ""+cursor.getString(cursor.getColumnIndex(Constants.C_GENRE))
                val tanggal = ""+cursor.getString(cursor.getColumnIndex(Constants.C_TANGGAL))
                val addedTimestamp = ""+cursor.getString(cursor.getColumnIndex(Constants.C_ADDED_TIMESTAMP))
                val updatedTimestamp = ""+cursor.getString(cursor.getColumnIndex(Constants.C_UPDATED_TIMESTAMP))

                val calendar1 = Calendar.getInstance(Locale.getDefault())
                calendar1.timeInMillis = addedTimestamp.toLong()
                val timeAdded = android.text.format.DateFormat.format("dd/MM/yyy hh:mm:aa", calendar1)

                val calendar2 = Calendar.getInstance(Locale.getDefault())
                calendar2.timeInMillis = updatedTimestamp.toLong()
                val timeUpdated = android.text.format.DateFormat.format("dd/MM/yyy hh:mm:aa", calendar2)

                judulTv.text = judul
                sinopsisTv.text = sinopsis
                durasiTv.text = durasi
                genreTv.text = genre
                tanggalTv.text = tanggal
                addedDateTv.text = timeAdded
                updatedDateTv.text = timeUpdated

                if(image == "null"){
                    profileIv.setImageResource(R.drawable.ic_person_black)
                }
                else{
                    profileIv.setImageURI(Uri.parse(image))
                }

            }while (cursor.moveToNext())
        }
        db.close()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
