package nanda.fikri.uts

object Constants {

    const val DB_NAME = "MY_RECORDS_DB"

    const val DB_VERSION = 1

    const val TABLE_NAME = "MY_RECORDS_TABLE"

    const val C_ID = "ID"
    const val C_JUDUL = "JUDUL"
    const val C_IMAGE = "IMAGE"
    const val C_SINOPSIS = "SINOPSIS"
    const val C_DURASI = "DURASI"
    const val C_GENRE = "GENRE"
    const val C_TANGGAL = "TANGGAL"
    const val C_ADDED_TIMESTAMP = "ADDED_TIME_STAMP"
    const val C_UPDATED_TIMESTAMP = "UPDATED_TIMESTAMP"

    const val CREATE_TABLE = (
            "CREATE TABLE " + TABLE_NAME + "("
                    + C_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + C_JUDUL + " TEXT,"
                    + C_IMAGE + " TEXT,"
                    + C_SINOPSIS + " TEXT,"
                    + C_DURASI + " TEXT,"
                    + C_GENRE + " TEXT,"
                    + C_TANGGAL + " TEXT,"
                    + C_ADDED_TIMESTAMP + " TEXT,"
                    + C_UPDATED_TIMESTAMP + " TEXT"
                    + ")"
            )

}