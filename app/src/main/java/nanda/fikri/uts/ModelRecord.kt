package nanda.fikri.uts

class ModelRecord (
    var id: String,
    var judul: String,
    var image: String,
    var sinopsis: String,
    var durasi: String,
    var genre: String,
    var tanggal: String,
    var addedTime: String,
    var updatedTime: String
)