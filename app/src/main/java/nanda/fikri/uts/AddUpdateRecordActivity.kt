package nanda.fikri.uts

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_add_update_record.*

class AddUpdateRecordActivity : AppCompatActivity() {

    private val CAMERA_REQUEST_CODE = 100
    private val STORAGE_REQUEST_CODE = 101

    private val IMAGE_PICK_CAMERA_CODE = 102
    private val IMAGE_PICK_GALLERY_CODE = 103

    private lateinit var cameraPermissions: Array<String>
    private lateinit var storagePermissions: Array<String>

    private var imageUri: Uri? = null
    private var id: String? = ""
    private var judul: String? = ""
    private var durasi: String? = ""
    private var genre: String? = ""
    private var tanggal: String? = ""
    private var sinopsis: String? = ""
    private var addedTime: String? = ""
    private var updatedTime: String? = ""

    private var isEditMode = false

    private var actionBar: ActionBar? = null
    lateinit var dbHelper:MyDbHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_update_record)

        actionBar = supportActionBar

        actionBar!!.title = "Add Record"

        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar!!.setDisplayShowHomeEnabled(true)

        val intent = intent
        isEditMode = intent.getBooleanExtra("isEditMode", false)
        if(isEditMode){
            actionBar!!.title = "Update Record"

            id = intent.getStringExtra("ID")
            judul = intent.getStringExtra("JUDUL")
            durasi = intent.getStringExtra("DURASI")
            genre = intent.getStringExtra("GENRE")
            tanggal = intent.getStringExtra("TANGGAL")
            sinopsis = intent.getStringExtra("SINOPSIS")
            imageUri = Uri.parse(intent.getStringExtra("IMAGE"))
            addedTime = intent.getStringExtra("ADDED_TIME")
            updatedTime = intent.getStringExtra("UPDATED_TIME")

            if(imageUri.toString() == "null"){
                provileIv.setImageResource(R.drawable.ic_person_black)
            }
            else{
                provileIv.setImageURI(imageUri)
            }
            judulEt.setText(judul)
            durasiEt.setText(durasi)
            genreEt.setText(genre)
            tanggalEt.setText(tanggal)
            sinopsisEt.setText(sinopsis)
        }
        else{
            actionBar!!.title = "Add Record"
        }

        dbHelper = MyDbHelper(this)

        cameraPermissions = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        storagePermissions = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        provileIv.setOnClickListener {
            imagePickDialog()
        }

        saveBtn.setOnClickListener {
            inputData()
        }
    }

    private fun inputData() {
        judul = "" + judulEt.text.toString().trim()
        durasi = "" + durasiEt.text.toString().trim()
        genre = "" + genreEt.text.toString().trim()
        tanggal = "" + tanggalEt.text.toString().trim()
        sinopsis = "" + sinopsisEt.text.toString().trim()

        if(isEditMode){
            val timeStamp = "${System.currentTimeMillis()}"
            dbHelper?.updateRecord(
                "$id",
                "$judul",
                "$imageUri",
                "$sinopsis",
                "$durasi",
                "$genre",
                "$sinopsis",
                "$addedTime",
                "$timeStamp"
            )

            Toast.makeText(this, "Updated...", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, MainActivity::class.java))
        }
        else{
            val timestamp = System.currentTimeMillis()
            val id = dbHelper.insertRecord(
                ""+judul,
                ""+imageUri,
                ""+sinopsis,
                ""+durasi,
                ""+genre,
                ""+tanggal,
                "$timestamp",
                "$timestamp"
            )
            Toast.makeText(this, "Record Added agains ID $id", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, MainActivity::class.java))
        }


    }

    private fun imagePickDialog(){
        val options = arrayOf("Camera", "Gallery")

        val builder = AlertDialog.Builder(this)

        builder.setTitle("Pick Image From")

        builder.setItems(options){dialog, which ->
            if(which==0){
                if(!checkCameraPermissions()){
                    requestCameraPermissions()
                }
                else{
                    pickFromCamera()
                }
            }
            else{
                if(!checkStoragePermission()){
                    requestStoragePermission()
                }
                else{
                    pickFromGallery()
                }
            }
        }

        builder.show()
    }

    private fun pickFromGallery(){
        val galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.type = "image/*"
        startActivityForResult(
            galleryIntent,
            IMAGE_PICK_GALLERY_CODE
        )
    }

    private fun requestStoragePermission(){
        ActivityCompat.requestPermissions(this, storagePermissions, STORAGE_REQUEST_CODE)
    }

    private fun checkStoragePermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun pickFromCamera(){
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "Image Title")
        values.put(MediaStore.Images.Media.DESCRIPTION, "Image Description")

        imageUri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        startActivityForResult(
            cameraIntent,
            IMAGE_PICK_CAMERA_CODE
        )
    }

    private fun requestCameraPermissions(){
        ActivityCompat.requestPermissions(this, cameraPermissions, CAMERA_REQUEST_CODE)
    }

    private fun checkCameraPermissions(): Boolean {
        val result = ContextCompat.checkSelfPermission(this,
            Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
        val results1 = ContextCompat.checkSelfPermission(this,
            Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED

        return result && results1
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            CAMERA_REQUEST_CODE->{
                if(grantResults.isNotEmpty()){
                    val cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    val storageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                    if(cameraAccepted && storageAccepted){
                        pickFromCamera()
                    }
                    else{
                        Toast.makeText(this, "Camera and Storage permissions are required", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            STORAGE_REQUEST_CODE->{
                if(grantResults.isNotEmpty()){
                    val storageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    if(storageAccepted){
                        pickFromGallery()
                    }
                    else{
                        Toast.makeText(this, "Storage permissions is required", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == IMAGE_PICK_GALLERY_CODE){
                CropImage.activity(data!!.data)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this)
            }
            else if(requestCode == IMAGE_PICK_CAMERA_CODE){
                CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this)
            }
            else if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
                val result =  CropImage.getActivityResult(data)
                if(resultCode == Activity.RESULT_OK){
                    val resultUri = result.uri
                    imageUri = resultUri

                    provileIv.setImageURI(resultUri)
                }
                else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                    val error = result.error
                    Toast.makeText(this, ""+error, Toast.LENGTH_SHORT).show()
                }
            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
