package nanda.fikri.uts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var dbHelper:MyDbHelper

    private val NEWEST_FIRST = "${Constants.C_ADDED_TIMESTAMP} DESC"
    private val OLDEST_FIRST = "${Constants.C_ADDED_TIMESTAMP} ASC"
    private val TITLE_ASC = "${Constants.C_JUDUL} ASC"
    private val TITLE_DESC = "${Constants.C_JUDUL} DESC"

    private var recentSortOrder = NEWEST_FIRST

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dbHelper = MyDbHelper(this)
        loadRecords(NEWEST_FIRST)

        addRecordBtn.setOnClickListener{
            val intent = Intent(this, AddUpdateRecordActivity::class.java)
            intent.putExtra("isEditMode", false)
            startActivity(Intent(this, AddUpdateRecordActivity::class.java))
        }
    }

    private fun loadRecords(orderBy:String) {
        recentSortOrder = orderBy
        val adapterRecord = AdapterRecord(this, dbHelper.getAllRecords(orderBy))

        recordsRv.adapter = adapterRecord
    }

    private fun searchRecords(query:String){
        val adapterRecord = AdapterRecord(this, dbHelper.searchRecords(query))

        recordsRv.adapter = adapterRecord
    }

    private fun sortDialog(){
        val options = arrayOf("Name Ascending", "Name Descending", "Newest", "Oldest")

        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Sort By")
            .setItems(options){_, which ->
                if(which==0){
                    loadRecords(TITLE_ASC)
                }
                else if(which==1){
                    loadRecords(TITLE_DESC)
                }
                else if(which==2){
                    loadRecords(NEWEST_FIRST)
                }
                else if(which==3){
                    loadRecords(OLDEST_FIRST)
                }
            }
            .show()
    }

    public override fun onResume() {
        super.onResume()
        loadRecords(recentSortOrder)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        val item = menu.findItem(R.id.action_search)
        val searchView = item.actionView as SearchView

        searchView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener{
            override fun onQueryTextChange(newText: String?): Boolean {
                if(newText != null){
                    searchRecords(newText)
                }
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                if(query != null){
                    searchRecords(query)
                }
                return true
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if(id==R.id.action_sort){
            sortDialog()
        }
        else if(id==R.id.action_deleteall){
            dbHelper.deleteAllRecords()
            onResume()
        }
        return super.onOptionsItemSelected(item)
    }

}
