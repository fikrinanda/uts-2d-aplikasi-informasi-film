package nanda.fikri.uts

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class MyDbHelper(context: Context?): SQLiteOpenHelper(
    context,
    Constants.DB_NAME,
    null,
    Constants.DB_VERSION
    ) {
        override fun onCreate(db: SQLiteDatabase) {
            db.execSQL(Constants.CREATE_TABLE)
        }

        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            db.execSQL("DROP TABLE IF EXISTS "+ Constants.TABLE_NAME)
            onCreate(db)
        }

        fun insertRecord(
            judul: String?,
            image: String?,
            sinopsis: String?,
            durasi: String?,
            genre: String?,
            tanggal: String?,
            addedTime: String?,
            updatedTime: String?
        ):Long{
            val db = this.writableDatabase
            val values = ContentValues()

            values.put(Constants.C_JUDUL, judul)
            values.put(Constants.C_IMAGE, image)
            values.put(Constants.C_SINOPSIS, sinopsis)
            values.put(Constants.C_DURASI, durasi)
            values.put(Constants.C_GENRE, genre)
            values.put(Constants.C_TANGGAL, tanggal)
            values.put(Constants.C_ADDED_TIMESTAMP, addedTime)
            values.put(Constants.C_UPDATED_TIMESTAMP, updatedTime)

            val id = db.insert(Constants.TABLE_NAME, null, values)

            db.close()

            return id
        }

    fun updateRecord(id: String,
                     judul: String?,
                     image: String?,
                     sinopsis: String?,
                     durasi: String?,
                     genre: String?,
                     tanggal: String?,
                     addedTime: String?,
                     updatedTime: String?):Long{
        val db = this.writableDatabase
        val values = ContentValues()

        values.put(Constants.C_JUDUL, judul)
        values.put(Constants.C_IMAGE, image)
        values.put(Constants.C_SINOPSIS, sinopsis)
        values.put(Constants.C_DURASI, durasi)
        values.put(Constants.C_GENRE, genre)
        values.put(Constants.C_TANGGAL, tanggal)
        values.put(Constants.C_ADDED_TIMESTAMP, addedTime)
        values.put(Constants.C_UPDATED_TIMESTAMP, updatedTime)

        return  db.update(Constants.TABLE_NAME,
            values,
            "${Constants.C_ID}=?",
            arrayOf(id)).toLong()
    }

    fun getAllRecords(orderBy:String):ArrayList<ModelRecord>{
        val recordList = ArrayList<ModelRecord>()

        val selectquery = "SELECT * FROM ${Constants.TABLE_NAME} ORDER BY $orderBy"
        val db = this.writableDatabase
        val cursor = db.rawQuery(selectquery, null)

        if(cursor.moveToFirst()){
            do{
                val modelRecord = ModelRecord(
                    ""+cursor.getInt(cursor.getColumnIndex(Constants.C_ID)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_JUDUL)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_IMAGE)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_SINOPSIS)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_DURASI)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_GENRE)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_TANGGAL)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_ADDED_TIMESTAMP)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_UPDATED_TIMESTAMP))
                )

                recordList.add(modelRecord)
            }while (cursor.moveToNext())
        }

        db.close()

        return recordList
    }

    fun searchRecords(query:String):ArrayList<ModelRecord>{
        val recordList = ArrayList<ModelRecord>()

        val selectquery = "SELECT * FROM ${Constants.TABLE_NAME} WHERE ${Constants.C_JUDUL} LIKE '%$query%'"
        val db = this.writableDatabase
        val cursor = db.rawQuery(selectquery, null)

        if(cursor.moveToFirst()){
            do{
                val modelRecord = ModelRecord(
                    ""+cursor.getInt(cursor.getColumnIndex(Constants.C_ID)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_JUDUL)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_IMAGE)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_SINOPSIS)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_DURASI)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_GENRE)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_TANGGAL)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_ADDED_TIMESTAMP)),
                    ""+cursor.getString(cursor.getColumnIndex(Constants.C_UPDATED_TIMESTAMP))
                )

                recordList.add(modelRecord)
            }while (cursor.moveToNext())
        }

        db.close()

        return recordList
    }

    fun recordCount():Int{
        val countQuery = "SELECT * FROM ${Constants.TABLE_NAME}"
        val db = this.readableDatabase
        val cursor = db.rawQuery(countQuery, null)
        val count = cursor.count
        cursor.close()
        return count
    }

    fun deleteRecord(id:String){
        val db = writableDatabase
        db.delete(
            Constants.TABLE_NAME,
            "${Constants.C_ID} =?",
            arrayOf(id)
        )
        db.close()
    }

    fun deleteAllRecords(){
        val db = writableDatabase
        db.execSQL("DELETE FROM ${Constants.TABLE_NAME}")
        db.close()
    }

}