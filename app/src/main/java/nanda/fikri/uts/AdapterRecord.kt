package nanda.fikri.uts

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdapterRecord(): RecyclerView.Adapter<AdapterRecord.HolderRecord>() {

    private var context: Context?=null
    private var recordList:ArrayList<ModelRecord>?=null

    lateinit var dbHelper: MyDbHelper

    constructor(context: Context?, recordList: ArrayList<ModelRecord>?) : this() {
        this.context = context
        this.recordList = recordList

        dbHelper = MyDbHelper(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderRecord {
        return HolderRecord(
            LayoutInflater.from(context).inflate(R.layout.row_record, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return recordList!!.size
    }

    override fun onBindViewHolder(holder: HolderRecord, position: Int) {
        val model = recordList!!.get(position)

        val id = model.id
        val judul = model.judul
        val image = model.image
        val sinopsis = model.sinopsis
        val durasi = model.durasi
        val genre = model.genre
        val tanggal = model.tanggal
        val addedTime = model.addedTime
        val updatedTime = model.updatedTime

        holder.nameTv.text = judul
        holder.durasiTv.text = durasi
        holder.genreTv.text = genre
        holder.tanggalTv.text = tanggal

        if(image == "null"){
            holder.profileIv.setImageResource(R.drawable.ic_person_black)
        }
        else{
            holder.profileIv.setImageURI(Uri.parse(image))
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(context, RecordDetailActivity::class.java)
            intent.putExtra("RECORD_ID", id)
            context!!.startActivity(intent)
        }

        holder.moreBtn.setOnClickListener {
            showMoreOptions(
                position,
                id,
                judul,
                durasi,
                genre,
                tanggal,
                sinopsis,
                image,
                addedTime,
                updatedTime
            )
        }
    }

    private fun showMoreOptions(
        position: Int,
        id: String,
        judul: String,
        durasi: String,
        genre: String,
        tanggal: String,
        sinopsis: String,
        image: String,
        addedTime: String,
        updatedTime: String
    ){
        val options = arrayOf("Edit", "Delete")

        val dialog: AlertDialog.Builder = AlertDialog.Builder(context)
        dialog.setItems(options) {dialog, which ->
            if(which==0){
                val intent = Intent(context, AddUpdateRecordActivity::class.java)
                intent.putExtra("ID", id)
                intent.putExtra("JUDUL", judul)
                intent.putExtra("DURASI", durasi)
                intent.putExtra("GENRE", genre)
                intent.putExtra("TANGGAL", tanggal)
                intent.putExtra("SINOPSIS", sinopsis)
                intent.putExtra("IMAGE", image)
                intent.putExtra("ADDED_TIME", addedTime)
                intent.putExtra("UPDATED_TIME", updatedTime)
                intent.putExtra("isEditMode", true)
                context!!.startActivity(intent)
            }
            else{
                dbHelper.deleteRecord(id)

                (context as MainActivity)!!.onResume()

            }
        }
        dialog.show()
    }

    inner class HolderRecord(itemView: View): RecyclerView.ViewHolder(itemView){

        var profileIv: ImageView = itemView.findViewById(R.id.profileIv)
        var nameTv: TextView = itemView.findViewById(R.id.judulTv)
        var durasiTv: TextView = itemView.findViewById(R.id.durasiTv)
        var genreTv: TextView = itemView.findViewById(R.id.genreTv)
        var tanggalTv: TextView = itemView.findViewById(R.id.tanggalTv)
        var moreBtn: ImageButton = itemView.findViewById(R.id.moreBtn)


    }
}